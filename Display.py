from GameBoard import *

def displayGame(boards):
    a = boards[0].getBoard()
    b = boards[1].getBoard()
    c = boards[2].getBoard()
    d = boards[3].getBoard()
    e = boards[4].getBoard()
    f = boards[5].getBoard()
    g = boards[6].getBoard()
    h = boards[7].getBoard()
    i = boards[8].getBoard()

    toReturn = "```       |       |       \n"
    toReturn += " " + a[0] + "|" + a[1] + "|" + a[2] + " | " + b[0] + "|" + b[1] + "|" + b[2] + " | " + c[0] + "|" + c[1] + "|" + c[2] + " \n"
    toReturn += " -+-+- | -+-+- | -+-+- \n"
    toReturn += " " + a[3] + "|" + a[4] + "|" + a[5] + " | " + b[3] + "|" + b[4] + "|" + b[5] + " | " + c[3] + "|" + c[4] + "|" + c[5] + " \n"
    toReturn += " -+-+- | -+-+- | -+-+- \n"
    toReturn += " " + a[6] + "|" + a[7] + "|" + a[8] + " | " + b[6] + "|" + b[7] + "|" + b[8] + " | " + c[6] + "|" + c[7] + "|" + c[8] + " \n"
    toReturn += "       |       |       \n"

    toReturn += "-------+-------+-------\n"

    toReturn += "       |       |       \n"
    toReturn += " " + d[0] + "|" + d[1] + "|" + d[2] + " | " + e[0] + "|" + e[1] + "|" + e[2] + " | " + f[0] + "|" + f[1] + "|" + f[2] + " \n"
    toReturn += " -+-+- | -+-+- | -+-+- \n"
    toReturn += " " + d[3] + "|" + d[4] + "|" + d[5] + " | " + e[3] + "|" + e[4] + "|" + e[5] + " | " + f[3] + "|" + f[4] + "|" + f[5] + " \n"
    toReturn += " -+-+- | -+-+- | -+-+- \n"
    toReturn += " " + d[6] + "|" + d[7] + "|" + d[8] + " | " + e[6] + "|" + e[7] + "|" + e[8] + " | " + f[6] + "|" + f[7] + "|" + f[8] + " \n"
    toReturn += "       |       |       \n"
    
    toReturn += "-------+-------+-------\n"

    toReturn += "       |       |       \n"
    toReturn += " " + g[0] + "|" + g[1] + "|" + g[2] + " | " + h[0] + "|" + h[1] + "|" + h[2] + " | " + i[0] + "|" + i[1] + "|" + i[2] + " \n"
    toReturn += " -+-+- | -+-+- | -+-+- \n"
    toReturn += " " + g[3] + "|" + g[4] + "|" + g[5] + " | " + h[3] + "|" + h[4] + "|" + h[5] + " | " + i[3] + "|" + i[4] + "|" + i[5] + " \n"
    toReturn += " -+-+- | -+-+- | -+-+- \n"
    toReturn += " " + g[6] + "|" + g[7] + "|" + g[8] + " | " + h[6] + "|" + h[7] + "|" + h[8] + " | " + i[6] + "|" + i[7] + "|" + i[8] + " \n"
    toReturn += "       |       |       ```"

    return toReturn

def displayCurrBoard(curr_board, curr_char):
    if (curr_board == '#'):
        curr_board = " anywhere."
    else:
        curr_board = " on board " + curr_board + "."
    
    if (curr_char == 'O'):
        curr_player = "O can play"
    else:
        curr_player = "X can play"

    return curr_player + curr_board
    

import discord
from discord.ext import commands
from GameBoard import *
from Display import *


client = commands.Bot(command_prefix = "!")


@client.command()
async def newGame(ctx):
    await ctx.send("Who is playing X's? (Type \"Me\")")
    while (True):
        msg = await client.wait_for('message')
        if (msg.content.lower().strip() == "me"):
            break
    x_player = msg.author

    await ctx.send("Who is playing O's? (Type \"Me\")")
    while (True):
        msg = await client.wait_for('message')
        if (msg.content.lower().strip() == "me"):
            break
    o_player = msg.author
    curr_player = o_player

    boards = (GameBoard(), GameBoard(), GameBoard(), GameBoard(), GameBoard(), GameBoard(), GameBoard(), GameBoard(), GameBoard())
    curr_board = '#'

    while (checkOverallWinner(boards) == ' '):

        if (curr_player == o_player):
            curr_player = x_player
            curr_char = 'X'
        else:
            curr_player = o_player
            curr_char = 'O'

        await ctx.send(displayGame(boards))
        await ctx.send(displayCurrBoard(curr_board, curr_char))

        invalid_message = True
        while (invalid_message):
            try:
                msg = await client.wait_for('message', check=lambda message: message.author == curr_player)
                #msg = await client.wait_for('message')
                msg = msg.content
                board_char = msg[0]
                board_index = ord(board_char) - 97
                square_index = ord(msg[1]) - 49

                if (board_index >= 0 and board_index < 9 and square_index >= 0 and square_index < 9):
                    if ((board_char == curr_board or curr_board == '#') and boards[board_index].getWinner() == ' ' and not boards[board_index].getOccupied(square_index)):
                        invalid_message = False
            except:
                pass
        
        boards[board_index].changeSquare(curr_char, square_index)
        
        if (boards[square_index].getWinner() != ' '):
            curr_board = '#'
        else:
            curr_board = chr(square_index + 97)

    if (checkOverallWinner(boards) == 'X'):
        await ctx.send("X has won!")
    else:
        await ctx.send("O has won!")

                
def checkOverallWinner(boards):
    a = boards[0].getWinner()
    b = boards[1].getWinner()
    c = boards[2].getWinner()
    d = boards[3].getWinner()
    e = boards[4].getWinner()
    f = boards[5].getWinner()
    g = boards[6].getWinner()
    h = boards[7].getWinner()
    i = boards[8].getWinner()

    if (a == b and b == c and a != ' '):
        return a
    elif (a == d and d == g and a != ' '):
        return a
    elif (a == e and e == i and a != ' '):
        return a
    elif (b == e and e == h and b != ' '):
        return b
    elif (c == f and f == i and c != ' '):
        return c
    elif (d == e and e == f and e != ' '):
        return e
    elif (g == h and h == i and g != ' '):
        return g
    elif (c == e and e == g and c != ' '):
        return c
    else:
        return ' '

client.run("NzkxMTI0NzgyMzQyOTk1OTY5.X-KmXg.bcNtbs2YBv8bTpyL8P3PMkGX5N4")

    


    


